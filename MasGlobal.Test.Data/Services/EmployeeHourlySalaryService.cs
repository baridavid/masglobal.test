﻿using MasGlobal.Test.Core.DTOs;
using MasGlobal.Test.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MasGlobal.Test.Data.Services
{
    public class EmployeeHourlySalaryService : IEmployee
    {
        private readonly double _salary;

        public EmployeeHourlySalaryService(double salary)
        {

            _salary = salary;
        }

        public double AnualSalaryEmployees()
        {

            return 120 * _salary * 12;
        }

        public Task<List<Employee>> GetEmployees(int id)
        {
            throw new NotImplementedException();
        }
    }
}
