﻿using MasGlobal.Test.Core.DTOs;
using MasGlobal.Test.Core.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;


namespace MasGlobal.Test.Data.Services
{
    public class EmployeeService : IEmployee
    {
        public double AnualSalaryEmployees()
        {
            throw new NotImplementedException();
        }

        public async Task<List<Employee>> GetEmployees(int id)
        {

            try
            {
                List<Employee> employeeList = new List<Employee>();

                var API_URL = Data.Constants.API.BaseURL;

                using (var client = new HttpClient())
                {

                    using (var response = await client.GetAsync(API_URL))
                    {

                        string apiResponse = await response.Content.ReadAsStringAsync();
                        employeeList = JsonConvert.DeserializeObject<List<Employee>>(apiResponse);

                    }

                }

                if (id != 0)
                    employeeList = employeeList.Where(p => p.Id == id).ToList();

                return employeeList;

            }
            catch (Exception ex)
            {

                throw;

            }



        }
    }
}
