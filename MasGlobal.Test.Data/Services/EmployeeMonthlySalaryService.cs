﻿using MasGlobal.Test.Core.DTOs;
using MasGlobal.Test.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.Test.Data.Services
{
    public class EmployeeMonthlySalaryService : IEmployee
    {

        private readonly double _salary;
        public EmployeeMonthlySalaryService(double salary)
        {

            _salary = salary;

        }

        public double AnualSalaryEmployees()
        {

            return _salary * 12;
        }

        public Task<List<Employee>> GetEmployees(int id)
        {
            throw new NotImplementedException();
        }
    }
}
