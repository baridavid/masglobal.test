﻿using MasGlobal.Test.Core.Interfaces;
using MasGlobal.Test.Data.Services;


namespace MasGlobal.Test.Core
{
    public class EmployeeFactory
    {

        const string CONTRACT_TYPE_NAME_HOURLY = "HourlySalaryEmployee";

        public IEmployee GetSalary(string contractTypeName, double monthlySalary, double hourlySalary)
        {

            if (contractTypeName.Equals(CONTRACT_TYPE_NAME_HOURLY))
            {

                return new EmployeeHourlySalaryService(hourlySalary);

            }
            else
            {
                return new EmployeeMonthlySalaryService(monthlySalary);

            }

        }


    }
}
