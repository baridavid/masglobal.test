﻿using MasGlobal.Test.Core;
using MasGlobal.Test.Core.Interfaces;
using MasGlobal.Test.Data.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace MasGlobal.Test.Data
{
    public static class ServiceRegistration
    {
        public static void AddDataLayer(this IServiceCollection services, IConfiguration configuration)
        {


            services.AddTransient<IEmployee, EmployeeService>();

            services.AddScoped<EmployeeFactory>();


        }


    }
}
