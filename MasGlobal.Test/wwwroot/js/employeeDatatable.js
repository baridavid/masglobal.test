﻿$(document).ready(function () {

    $("#employeeDatatable").DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/api/Employee",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs": [{
            "targets": [0],
            "visible": true,
            "searchable": false
        }],
        "oLanguage": {
            "sSearch": "Get Employees by id : "
        }
        ,
        "columns": [
            { "data": "id", "name": "Id", "autoWidth": true },
            { "data": "name", "name": "Name", "autoWidth": true },
            { "data": "contractTypeName", "name": "ContractTypeName", "autoWidth": true },
            { "data": "roleId", "name": "RoleId", "autoWidth": true },
            { "data": "roleName", "name": "RoleName", "autoWidth": true },
            { "data": "roleDescripcion", "name": "RoleDescripcion", "autoWidth": true },
            { "data": "hourlySalary", "name": "HourlySalary", "autoWidth": true },
            { "data": "monthlySalary", "name": "MonthlySalary", "autoWidth": true },
            { "data": "anualSalary", "name": "AnualSalary", "autoWidth": true },

       
        ]
    });


}); 



