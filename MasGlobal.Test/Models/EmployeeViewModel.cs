﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MasGlobal.Test.Models
{
    public class EmployeeViewModel
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string ContractTypeName { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public string RoleDescripcion { get; set; }

        public double HourlySalary { get; set; }

        public double MonthlySalary { get; set; }

        public double AnualSalary { get; set; }

    }
}
