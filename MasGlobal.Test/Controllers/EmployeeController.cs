﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasGlobal.Test.Core;
using MasGlobal.Test.Core.DTOs;
using MasGlobal.Test.Core.Interfaces;
using MasGlobal.Test.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasGlobal.Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployee employee;
        private readonly EmployeeFactory _employeeFactory;


        public EmployeeController(IEmployee employee, EmployeeFactory employeeFactory)
        {

            this.employee = employee;
            _employeeFactory = employeeFactory;


        }
        [HttpPost]
        public async Task<IActionResult> GetEmployees()
        {

            List<Employee> employeeList;

            try
            {

                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;


                if (!string.IsNullOrEmpty(searchValue))
                {
                    employeeList = await employee.GetEmployees(Convert.ToInt32(searchValue));

                }
                else
                {
                    employeeList = await employee.GetEmployees(0);

                }


                var model = (from p in employeeList
                             select new EmployeeViewModel()
                             {
                                 Id = p.Id,
                                 Name = p.Name,
                                 ContractTypeName = p.ContractTypeName,
                                 RoleId = p.RoleId,
                                 RoleName = p.RoleName,
                                 RoleDescripcion = p.RoleDescripcion,
                                 HourlySalary = p.HourlySalary,
                                 MonthlySalary = p.MonthlySalary,
                                 AnualSalary = _employeeFactory.GetSalary(p.ContractTypeName, p.MonthlySalary, p.HourlySalary).AnualSalaryEmployees()
                             }).ToList();

                var employeeData = model;

                recordsTotal = employeeData.Count();

                var data = employeeData.Skip(skip).Take(pageSize).ToList();

                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };

                return Ok(jsonData);



            }
            catch (Exception ex)
            {
                throw;
            }


        }

    }
}