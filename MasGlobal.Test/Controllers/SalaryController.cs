﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasGlobal.Test.Core;
using MasGlobal.Test.Core.DTOs;
using MasGlobal.Test.Core.Interfaces;
using MasGlobal.Test.Data.Services;
using MasGlobal.Test.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasGlobal.Test.Controllers
{
    public class SalaryController : Controller
    {
        private readonly IEmployee _employee;

        private readonly EmployeeFactory _employeeFactory;

        public SalaryController(IEmployee employee, EmployeeFactory employeeFactory)
        {

            _employee = employee;
            _employeeFactory = employeeFactory;
        }

        public async Task<IActionResult> Index(EmployeeViewModel emp)
        {

           
                var employees = await _employee.GetEmployees(emp.Id);

                var model = (from p in employees
                             select new EmployeeViewModel()
                             {
                                 Id = p.Id,
                                 Name = p.Name,
                                 ContractTypeName = p.ContractTypeName,
                                 RoleId = p.RoleId,
                                 RoleName = p.RoleName,
                                 RoleDescripcion = p.RoleDescripcion,
                                 HourlySalary = p.HourlySalary,
                                 MonthlySalary = p.MonthlySalary,
                                 AnualSalary = _employeeFactory.GetSalary(p.ContractTypeName, p.MonthlySalary, p.HourlySalary).AnualSalaryEmployees()


                             }).ToList();

                ViewBag.EmployeeList = model;

                return View();

        
        
        }
    }
}