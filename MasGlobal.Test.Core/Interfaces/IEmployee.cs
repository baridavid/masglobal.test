﻿using MasGlobal.Test.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.Test.Core.Interfaces
{
    public  interface IEmployee
    {

         double AnualSalaryEmployees();

         Task<List<Employee>> GetEmployees(int id);

    }
}
