using MasGlobal.Test.Core;
using Xunit;

namespace UnitTests
{
    public class EmployeeFactoryTest
    {

        private EmployeeFactory _factory;

        private readonly double _testMonthlySalary = 80000;

        private readonly double _testHourlySalary = 60000;

        private readonly string _testContractTypeNameHourly = "HourlySalaryEmployee";

        private readonly string _testContractTypeNameMonthly = "MonthlySalaryEmployee";


        public EmployeeFactoryTest()
        {

            _factory = new EmployeeFactory();
        }


        [Fact]
        public void ShouldGetAnualSalaryHourly()
        {

            var salary = _factory.GetSalary(_testContractTypeNameHourly, _testMonthlySalary, _testHourlySalary).AnualSalaryEmployees();

            Assert.Equal("86400000", salary.ToString());


        }
        [Fact]
        public void ShouldGetAnualSalaryMonthly()
        {

            var salary = _factory.GetSalary(_testContractTypeNameMonthly, _testMonthlySalary, _testHourlySalary).AnualSalaryEmployees();

            Assert.Equal("960000", salary.ToString());

        }
    }
}
